<?php

namespace Drupal\ofd_ferma\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining OFD.Ferma receipt entities.
 *
 * @ingroup ofd_ferma
 */
interface ofd_ferma_receiptInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the OFD.Ferma ReceiptId.
   *
   * @return string
   *   ID of the OFD.Ferma receipt.
   */
  public function getFermaID();

  /**
   * Sets the OFD.Ferma receipt name.
   *
   * @param string $name
   *   The OFD.Ferma receipt name.
   *
   * @return \Drupal\ofd_ferma\Entity\ofd_ferma_receiptInterface
   *   The called OFD.Ferma receipt entity.
   */
  public function setFermaID($fid);

  /**
   * Gets the OFD.Ferma receipt creation timestamp.
   *
   * @return int
   *   Creation timestamp of the OFD.Ferma receipt.
   */
  public function getCreatedTime();

  /**
   * Sets the OFD.Ferma receipt creation timestamp.
   *
   * @param int $timestamp
   *   The OFD.Ferma receipt creation timestamp.
   *
   * @return \Drupal\ofd_ferma\Entity\ofd_ferma_receiptInterface
   *   The called OFD.Ferma receipt entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the OFD.Ferma receipt status.
   *
   * @return text
   *   Status of the OFD.Ferma receipt.
   */
  public function getStatus();

  /**
   * Sets the status of a OFD.Ferma receipt.
   *
   * @param text $published
   *   Status of the OFD.Ferma receipt.
   *
   * @return \Drupal\ofd_ferma\Entity\ofd_ferma_receiptInterface
   *   The called OFD.Ferma receipt entity.
   */
  public function setStatus($status);

  /**
   * Returns the OFD.Ferma receipt status.
   *
   * @return serial
   *   Device info of the OFD.Ferma receipt.
   */
  public function getDevice();

  /**
   * Sets the status of a OFD.Ferma receipt.
   *
   * @param serial $device
   *   Device info of the OFD.Ferma receipt.
   *
   * @return \Drupal\ofd_ferma\Entity\ofd_ferma_receiptInterface
   *   The called OFD.Ferma receipt entity.
   */
  public function setDevice($device);
}
