<?php

namespace Drupal\ofd_ferma\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the OFD.Ferma receipt entity.
 *
 * @ingroup ofd_ferma
 *
 * @ContentEntityType(
 *   id = "ofd_ferma_receipt",
 *   label = @Translation("OFD.Ferma receipt"),
 *   bundle_label = @Translation("OFD.Ferma receipt type"),
 *   handlers = {
 *     "views_data" = "Drupal\ofd_ferma\Entity\ofd_ferma_receiptViewsData",
 *   },
 *   base_table = "ofd_ferma_receipt",
 *   entity_keys = {
 *     "id" = "rid",
 *   },
 * )
 */
class ofd_ferma_receipt extends ContentEntityBase implements ofd_ferma_receiptInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getFermaID() {
    return $this->get('fid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFermaID($fid) {
    $this->set('fid', $fid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModifiedTime() {
    return $this->get('changed')->value;
  }
  /**
   * {@inheritdoc}
   */
  public function setModifiedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDevice() {
    return unserialize($this->get('device'));
  }

  /**
   * {@inheritdoc}
   */
  public function setDevice($device) {
    $this->set('device', serialize($device));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
//    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['rid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('rid'))
      ->setDescription(t('Receipt ID.'))
      ->setReadOnly(TRUE);

    $fields['fid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('fid'))
      ->setDescription(t('OFD.Ferma ReceiptId'))
      ->setSettings([
        'max_length' => 36,
        'text_processing' => 0,
      ]);
    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('type'))
      ->setDescription(t('Receipt type'))
      ->setCardinality(1)
      ->setSettings([
        'max_length' => 13,
        'text_processing' => 0,
        'allowed_values_function' => ['\Drupal\ofd_ferma\Entity\ofd_ferma_receipt', 'getAvailableTypes'],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ]);
    $fields['status'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('status'))
      ->setDescription(t('Receipt status'))
      ->setCardinality(1)
      ->setSetting('allowed_values_function', ['\Drupal\ofd_ferma\Entity\ofd_ferma_receipt', 'getAvailableStatus'])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ]);
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('created'))
      ->setDescription(t('The time that the receipt was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('changed'))
      ->setDescription(t('The time that the receipt was last edited.'));

    $fields['tin'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('TIN'))
      ->setDescription(t('Taxpayer ID number.'));

    $fields['device'] = BaseFieldDefinition::create('map')
      ->setLabel(t('device info'))
      ->setDescription(t('OFD.Ferma device info'));

    return $fields;
  }

  /**
   * Gets the allowed values for the 'type' base field.
   *
   * @return array
   *   The allowed values.
   */
  public static function getAvailableTypes() {
    return _ofd_ferma_type_list();
  }

  /**
   * Gets the allowed values for the 'type' base field.
   *
   * @return array
   *   The allowed values.
   */
  public static function getAvailableStatus() {
    return _ofd_ferma_status_list();
  }
}
