<?php

namespace Drupal\ofd_ferma\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for OFD.Ferma receipt entities.
 */
class ofd_ferma_receiptViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
