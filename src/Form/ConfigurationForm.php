<?php

namespace Drupal\ofd_ferma\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ofd_ferma_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('ofd_ferma.settings');

    $form['login'] = [
      '#type' => 'textfield',
      '#title' => 'Login',
      '#description' => $this->t('OFD.Ferma API login'),
      '#default_value' => $config->get('login'),
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => 'Password',
      '#description' => $this->t('OFD.Ferma API password'),
      '#default_value' => $config->get('password'),
    ];
    $form['tin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TIN'),
      '#description' => $this->t('Taxpayer ID number'),
      '#default_value' => $config->get('tin'),
    ];
    $form['tax_system'] = [
      '#type' => 'select',
      '#title' => t('Taxation system'),
      '#default_value' => $config->get('tax_system'),
      '#options' => _ofd_ferma_tax_system_list(),
    ];
    $form['tax_default'] = [
      '#type' => 'select',
      '#title' => $this->t('Default tax rate'),
      '#default_value' => $config->get('tax_default'),
      '#options' => _ofd_ferma_tax_list(),
    ];
    $form['final_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Final status'),
      '#default_value' => $config->get('final_status'),
      '#options' => _ofd_ferma_status_list(),
    ];
    $form['cron_length'] = [
      '#type' => 'select',
      '#title' => $this->t('Cron batch size'),
      '#description' => $this->t('Set how many items will be updated at once when receipts status update during a cron run.'),
      '#options' => [10 => '10', 25 => '25', 50 => '50'],
      '#default_value' => $config->get('cron_length'),
    ];
    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail for error messages'),
      '#description' => $this->t('Leave empty for disabling'),
      '#default_value' => $config->get('mail'),
    ];
    $warning = $this->t('INSECURITY! The "SSL certificate problem" is often caused by an outdated or missing bundle of CA root certificates, in your PHP configuration. ' .
                        'Please fix your php.ini configuration instead ignoring ssl certificate checking. ' .
                        'Remember that your private data may hijacked by a MITM attack.');
    $form['warning'] = [
      '#type' => 'item',
      '#title' => $this->t('Skip SSL certificate check'),
      '#markup' => '<div class="messages messages--warning">' . $warning . '</div>',
    ];
    $form['skip_ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I understand what I\'m doing and I take responsibility for the possible consequences'),
      '#default_value' => $config->get('skip_ssl'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ofd_ferma.settings')
      ->set('login', $form_state->getValue('login'))
      ->set('password', $form_state->getValue('password'))
      ->set('tin', $form_state->getValue('tin'))
      ->set('tax_system', $form_state->getValue('tax_system'))
      ->set('tax_default', $form_state->getValue('tax_default'))
      ->set('final_status', $form_state->getValue('final_status'))
      ->set('cron_length', $form_state->getValue('cron_length'))
      ->set('mail', $form_state->getValue('mail'))
      ->set('skip_ssl', $form_state->getValue('skip_ssl'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ofd_ferma.settings',
    ];
  }
}
