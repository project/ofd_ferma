<?php

namespace Drupal\ofd_ferma\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;

/**
 * Provides a 'Create OFD.Ferma unmanaged receipt' action.
 *
 * @RulesAction(
 *   id = "ofd_ferma_create_receipt_unmanaged",
 *   label = @Translation("Create OFD.Ferma unmanaged receipt"),
 *   category = @Translation("OFD.Ferma"),
 *   context = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Receipt type"),
 *       description = @Translation("Set one of: Income, IncomeReturn, Expense, ExpenseReturn"),
 *     ),
 *     "invoice_id" = @ContextDefinition("string",
 *       label = @Translation("Invoice ID"),
 *       description = @Translation("Must be unique per receipt type.")
 *     ),
 *     "mail" = @ContextDefinition("string",
 *       label = @Translation("Customer e-mail"),
 *       required = FALSE,
 *       allow_null = TRUE
 *     ),
 *     "phone" = @ContextDefinition("string",
 *       label = @Translation("Customer phone"),
 *       required = FALSE,
 *       allow_null = TRUE
 *     ),
 *     "item_label" = @ContextDefinition("string",
 *       label = @Translation("Receipt item label")
 *     ),
 *     "item_price" = @ContextDefinition("float",
 *       label = @Translation("Receipt item price")
 *     ),
 *     "item_qty" = @ContextDefinition("float",
 *       label = @Translation("Receipt item quantity")
 *     ),
 *     "item_vat" = @ContextDefinition("string",
 *       label = @Translation("Receipt item tax rate"),
 *       description = @Translation("Set one of: Vat0, Vat10, Vat18, CalculatedVat10110, CalculatedVat18118 or leave empty for default"),
 *       required = FALSE,
 *       allow_null = TRUE,
 *       assignment_restriction = "input"
 *     )
 *   },
 *   provides = {
 *     "receipt" = @ContextDefinition("entity:ofd_ferma_receipt",
 *       label = @Translation("OFD.Ferma receipt")
 *     )
 *   }
 * )
 *
 */
class CreateReceiptUnmanaged extends RulesActionBase {
  protected function doExecute($type, $invoice_id, $mail, $phone, $label, $price, $qty, $vat) {
    $config = \Drupal::config('ofd_ferma.settings');
    $item = [
      'Label' => $label,
      'Price' => $price,
      'Quantity' => $qty,
      'Amount' => $price * $qty,
      'Vat' => $vat == '0' ? $config->get('tax_default') : $vat,
    ];
    $items = [
      0 => (object) $item,
    ];
    $this->setProvidedValue('receipt', ofd_ferma_create_receipt($type, $invoice_id, $mail, $phone, $items));
  }
}