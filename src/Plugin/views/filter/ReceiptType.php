<?php

/**
 * @file
 * Definition of Drupal\ofd_ferma\Plugin\views\filter\ReceiptType.
 */

namespace Drupal\ofd_ferma\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;

/**
 * OFD.Ferma receipt type filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ofd_ferma_receipt_type_filter")
 */
class ReceiptType extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * Override the query so that no filtering takes place if the user doesn't
   * select any options.
   */
  public function query() {
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * Skip validation if no options have been chosen so we can use it as a
   * non-filter.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions() {
    return _ofd_ferma_type_list();
  }
}