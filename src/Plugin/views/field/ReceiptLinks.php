<?php

/**
 * @file
 * Definition of Drupal\ofd_ferma\Plugin\views\field\ReceiptLinks.
 */

namespace Drupal\ofd_ferma\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ofd_ferma_field_links")
 */
class ReceiptLinks extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['rid'] = ['default' => 'rid'];
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $all_fields = $this->view->display_handler->getFieldLabels();
    $field_options = array_slice($all_fields, 0, array_search($this->options['id'], array_keys($all_fields)));
    $form['rid'] = [
      '#type' => 'radios',
      '#title' => t('Select receipt ID (rid) field'),
      '#options' => $field_options,
      '#default_value' => $this->options['rid'],
      '#required' => TRUE,
    ];
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $field_rid = $this->options['rid'];
    if (isset($values->{$field_rid})) {
      $rid = $values->{$field_rid};
      $entity = \Drupal::entityManager()->getStorage('ofd_ferma_receipt')->load($rid)->toArray();
      if (!empty($entity['device'][0]) && !empty($entity['tin'][0]['value'])) {
        $data = $entity['device'][0];
        $uri = implode('/', [$entity['tin'][0]['value'], $data['RNM'], $data['FN'], $data['FDN'], $data['FPD']]);
        $link = [
          '#type' => 'link',
          '#url' => \Drupal\Core\Url::fromUri(OFD_FERMA_RENDER_URL . $uri),
          '#title' => $this->t('View'),
        ];
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists('colorbox_load')) {
          $link['#attributes']['class'] = ['colorbox'];
        }
        else {
          $link['#attributes']['target'] = '_blank';
        }
        return $link;
      }
    }
    else {
      return NULL;
    }
  }
}
