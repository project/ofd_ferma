<?php

/**
 * @file
 * Definition of Drupal\ofd_ferma\Plugin\views\field\DeviceInfo.
 */

namespace Drupal\ofd_ferma\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ofd_ferma_field_device")
 */
class DeviceInfo extends FieldPluginBase {
  public function render(ResultRow $values) {
    $value = $values->{$this->field_alias};
    if ($data = unserialize($value)) {
      $vars = [
        '#type' => 'table',
        '#rows' => [],
      ];
      foreach ($data as $key => $value) {
        if (in_array($key, ['RNM', 'FN'])) {
          $vars['#rows'][] = [$key, $value];
        }
      }
      return $vars;
    }
    return NULL;
  }
}
