<?php


function ofd_ferma_views_data_alter(&$data) {
  $data['ofd_ferma_receipt']['links'] = [
    'title' => t('View receipt'),
    'help' => t('Link for review rendered receipt'),
    'field' => [
      'id' => 'ofd_ferma_field_links',
    ],
  ];
  $data['ofd_ferma_receipt']['device'] = [
    'title' => t('Device info'),
    'help' => t('Detailed info about cash registry device.'),
    'field' => [
      'id' => 'ofd_ferma_field_device',
    ],
  ];
  $data['ofd_ferma_receipt']['type']['filter'] = [
    'field' => 'type',
    'id' => 'ofd_ferma_receipt_type_filter',
  ];
  $data['ofd_ferma_receipt']['status']['filter'] = [
    'field' => 'status',
    'id' => 'ofd_ferma_receipt_status_filter',
  ];
}
