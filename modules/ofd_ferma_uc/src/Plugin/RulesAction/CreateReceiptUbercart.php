<?php

namespace Drupal\ofd_ferma_uc\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;

/**
 * Provides a 'Create OFD.Ferma receipt from Ubercart order' action.
 *
 * @RulesAction(
 *   id = "ofd_ferma_uc_create_receipt",
 *   label = @Translation("Create OFD.Ferma receipt from Ubercart order"),
 *   category = @Translation("OFD.Ferma"),
 *   context = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Receipt type"),
 *       description = @Translation("Set one of: Income, IncomeReturn, Expense, ExpenseReturn"),
 *     ),
 *     "order" = @ContextDefinition("entity",
 *       label = @Translation("Ubercart order"),
 *     ),
 *     "mail" = @ContextDefinition("any",
 *       label = @Translation("Customer e-mail"),
 *       required = FALSE,
 *       allow_null = TRUE
 *     ),
 *     "phone" = @ContextDefinition("any",
 *       label = @Translation("Customer phone"),
 *       required = FALSE,
 *       allow_null = TRUE
 *     ),
 *     "items_vat" = @ContextDefinition("string",
 *       label = @Translation("Receipt item tax rate"),
 *       description = @Translation("Set one of: Vat0, Vat10, Vat18, CalculatedVat10110, CalculatedVat18118 or leave empty for default"),
 *       required = FALSE,
 *       allow_null = TRUE,
 *       assignment_restriction = "input"
 *     )
 *   }
 * )
 *
 */
class CreateReceiptUbercart extends RulesActionBase {

  protected function doExecute($type, $order, $mail, $phone, $items_vat) {
    $vat = empty($items_vat) ? '0' : $items_vat;
    $items = ofd_ferma_uc_get_items($order, $vat);
    $receipt = ofd_ferma_create_receipt($type, $order->order_id->value, $mail, $phone, $items);
    if ($receipt) {
      ofd_ferma_uc_receipt_set_order($receipt, $order->order_id->value);
    }
    else {
      _ofd_ferma_log('uc_create_receipt', t('Fail to create receipt for order @order', array('@order' => $order->order_id->value)));
    }
  }
}
