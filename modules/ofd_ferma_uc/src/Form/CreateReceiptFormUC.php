<?php

namespace Drupal\ofd_ferma_uc\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CreateReceiptFormUC extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ofd_ferma_uc_create_receipt';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $order_id = NULL) {
    $types = _ofd_ferma_type_list();
    if ($receipts = ofd_ferma_uc_order_receipts($order_id)) {
      foreach ($receipts as $receipt) {
        $arr = $receipt->toArray();
        foreach ($types as $id => $label) {
          if ($id == $arr['type'][0]['value']) {
            unset($types[$id]);
            break;
          }
        }
      }
    }
    $form['#prefix'] = '<div class="clearfix"></div><div>';
    $form['#suffix'] = '</div>';
    $form['wrapper'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Create OFD.Ferma receipt'),
    );
    $load = \Drupal::entityTypeManager()->getStorage('uc_order')->load($order_id);
    if ($types && $load) {
      $order = $load->toArray();
      $config = \Drupal::config('ofd_ferma.settings');
      $form['order_id'] = array(
        '#type' => 'value',
        '#value' => $order_id,
      );
      $form['wrapper']['type'] = array(
        '#type' => 'select',
        '#title' => t('Receipt type'),
        '#default_value' => '',
        '#options' => $types,
      );
      $form['wrapper']['mail'] = array(
        '#title' => t('Customer e-mail'),
        '#type' => 'textfield',
        '#default_value' => $order['primary_email'][0]['value'],
      );
      $form['wrapper']['phone'] = array(
        '#title' => t('Customer phone'),
        '#type' => 'textfield',
      );
      $form['wrapper']['tax_default'] = array(
        '#type' => 'select',
        '#title' => t('Default tax rate'),
        '#default_value' => $config->get('tax_default'),
        '#options' => _ofd_ferma_tax_list(),
      );
      $form['wrapper']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Create receipt'),
      );
    }
    else {
      $form['wrapper']['message'] = array(
        '#type' => 'markup',
        '#markup' => $this->t('There are no available types for creating a receipt'),
      );
    }
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['mail']) && empty($values['phone'])) {
      $form_state->setErrorByName('mail', $this->t('Must be specified e-mail or phone'));
      $form_state->setErrorByName('phone');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $order = \Drupal::entityTypeManager()->getStorage('uc_order')->load($values['order_id']);
    $items = ofd_ferma_uc_get_items($order, $values['tax_default']);
    $receipt = ofd_ferma_create_receipt($values['type'], $order->order_id->value, $values['mail'], $values['phone'], $items);
    if ($receipt) {
      ofd_ferma_uc_receipt_set_order($receipt, $order->order_id->value);
      drupal_set_message($this->t('The receipt was created'), 'status');
    }
    else {
      drupal_set_message($this->t('Unable create receipt. See log for more info'), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ofd_ferma_uc.create_receipt',
    ];
  }
}
