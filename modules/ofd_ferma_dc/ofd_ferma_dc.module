<?php

function ofd_ferma_dc_preprocess_page(&$vars) {
  $path = \Drupal::service('path.current')->getPath();
  $args = explode('/', $path);
  array_shift($args);
  if (count($args) == 5 && $args[0] == 'admin' && $args[1] == 'commerce' &&
      $args[2] == 'orders' && is_numeric($args[3]) && $args[4] == 'payments') {

    if(\Drupal::currentUser()->hasPermission('view register of cash receipts')) {
      $list = views_embed_view('ofd_ferma_dc_receipts', 'block_order_receipts');
      $vars['page']['content']['ofd_ferma_dc_view'] = $list;
    }
    if(\Drupal::currentUser()->hasPermission('create a cash receipt')) {
      $vars['page']['content']['ofd_ferma_dc_form'] = \Drupal::formBuilder()->getForm('\Drupal\ofd_ferma_dc\Form\CreateReceiptFormDC', $args[3]);
    }
  }
}

function ofd_ferma_dc_get_items($order, $items_vat) {
  $config = \Drupal::config('ofd_ferma.settings');
  $availiable_vat = _ofd_ferma_tax_list();
  $taxes = \Drupal::service('module_handler')->moduleExists('uc_tax');
  $items = [];
  foreach($order->order_items as $i) {
    $line_item = \Drupal::entityTypeManager()->getStorage('commerce_order_item')->load($i->target_id);
    $price = $line_item->unit_price->number;
    $vat = NULL;
    $tax = '';
    if ($taxes) {
      $product = $line_item->getPurchasedEntity();
      if ($base_price = $product->price->number) {
        $rate = intval(($price / $base_price - 1) * 100);
        $tax = "CalculatedVat$rate" . "1$rate";
        if (array_key_exists($tax, $availiable_vat)) {
          $vat = $tax;
        }
      }
    }
    if (!$vat) {
      $vat = $items_vat == '0' ? $config->get('tax_default') : $items_vat;
    }
    $item = [
      'Label' => $line_item->title->value,
      'Price' => $price,
      'Quantity' => $line_item->quantity->value,
      'Amount' => $line_item->total_price->number,
      'Vat' => $vat,
    ];
    $items[] = (object) $item;
  }
  foreach($order->shipments as $s) {
    $shipment = \Drupal::entityTypeManager()->getStorage('commerce_shipment')->load($s->target_id);
    $item = [
      'Label' => $shipment->title->value,
      'Price' => $shipment->amount->number,
      'Quantity' => 1,
      'Amount' => $shipment->amount->number,
      'Vat' => $items_vat == '0' ? $config->get('tax_default') : $items_vat,
    ];
    $items[] = (object) $item;
  }
  return $items;
}

function ofd_ferma_dc_receipt_set_order($receipt, $order_id) {
  $receipt->ofd_ferma_commerce_order->target_id = $order_id;
  $receipt->save();
}

function ofd_ferma_dc_order_receipts($order_id) {
  $query = \Drupal::entityQuery('ofd_ferma_receipt');
  $query->condition('ofd_ferma_commerce_order', $order_id);
  $result = $query->execute();
  return ($result) ? \Drupal::entityTypeManager()->getStorage('ofd_ferma_receipt')->loadMultiple(array_keys($result)) : NULL;
}
